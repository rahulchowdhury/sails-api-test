/**
 * WelcomeController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  greetUser: (req, res) => {
    res.send({ message: "Welcome to Sails.js API!" });
  }
};
